package com.datevalidator;


import com.datevalidator.dayscounter.DaysCounterService;
import com.datevalidator.model.DatesToCheckObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController implements ErrorController {


    @Autowired
    DaysCounterService daysCounterService;

    private static final String PATH = "/error";


    @GetMapping("/")
    public String root(Model model) {
        model.addAttribute("datesToCheckObject", new DatesToCheckObject());
        return "index";
    }

    @PostMapping("/getNumberOfDays")
    public String getNumberOfDays(@ModelAttribute DatesToCheckObject dateToCheckObject,
                                  Model model) {
        Integer daysBetween = daysCounterService.getNumberOfDays(dateToCheckObject);
        if(daysBetween < 0) {
            model.addAttribute("errorMessage", true);
        } else {
            model.addAttribute("numberOfDays", daysBetween);
        }

        return "index";
    }

    @RequestMapping("/error")
    public String error(Model model) {
        model.addAttribute("errorMessage", true);
        return "error";
    }

    @Override
    public String getErrorPath() {
        return PATH;
    }
}
