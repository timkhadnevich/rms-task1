package com.datevalidator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RMSTestOneApplication {

	public static void main(String[] args) {
		SpringApplication.run(RMSTestOneApplication.class, args);
	}
}
