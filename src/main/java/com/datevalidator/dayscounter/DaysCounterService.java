package com.datevalidator.dayscounter;

import com.datevalidator.model.DatesToCheckObject;



public interface DaysCounterService {

    /**
     * Returns number of days from DatesToCheckObject
     * @param datesToCheckObject
     * @return
     */
    Integer getNumberOfDays(DatesToCheckObject datesToCheckObject);

}
