package com.datevalidator.dayscounter;

import com.datevalidator.model.DatesToCheckObject;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class DaysCounterServiceImpl implements DaysCounterService {

    @Override
    public Integer getNumberOfDays(DatesToCheckObject datesToCheckObject) {
        return daysBetween(datesToCheckObject.getStartDate(), datesToCheckObject.getEndDate());
    }


    private Integer daysBetween(Date d1, Date d2){
        return (int)( (d2.getTime() - d1.getTime()) / (1000 * 60 * 60 * 24));
    }
}
