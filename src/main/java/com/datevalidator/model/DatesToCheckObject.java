package com.datevalidator.model;


import org.springframework.format.annotation.DateTimeFormat;
import java.util.Date;


public class DatesToCheckObject {

    @DateTimeFormat(pattern="MM/dd/yyyy")
    private Date startDate;

    @DateTimeFormat(pattern="MM/dd/yyyy")
    private Date endDate;

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
}
