package com.datevalidator;

import com.datevalidator.dayscounter.DaysCounterService;
import com.datevalidator.model.DatesToCheckObject;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DaysCounterServiceTests {

    @Autowired
    DaysCounterService daysCounterService;

    @Test
    public void testCounter() {
        DatesToCheckObject testObject = new DatesToCheckObject();
        testObject.setEndDate(new Date(2002,11,11));
        testObject.setStartDate(new Date(2000,11,11));
        Assert.assertEquals(Integer.valueOf(730), daysCounterService.getNumberOfDays(testObject));
    }

}
